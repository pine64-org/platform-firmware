# Purpose of this repository

Various Pine64 devices use different methods of booting.
Some have dedicated boot storage like an SPI module or eMMC boot partitions, others like the PinePhone Pro dev and Explorer Editions have neither and require platform firmware to be installed on shared storage.
To prevent different parties from using different methods and possibly overwriting each other's platform firmware, a specification is needed.

This is a known problem for all ARM devices, therefore ARM introduced the [Embedded Base Boot Requirements](https://arm-software.github.io/ebbr/) specification.
This specification was written to have a common standard for booting ARM devices through UEFI, protective storages, etc.
You can read the EBBR specification here: https://arm-software.github.io/ebbr/

We summarize this specification (v2.0) below for distros:

- MUST NOT install the platform firmware themselves. Hardware vendors should do this at the factory by flashing an open platform firmware with UEFI support like U-boot, Tow-Boot, Tianocore/EDK2.
This is done to make the chance of bricking a device as low as possible by flashing only when absolutely required.
In case the hardware vendor does not comply with the EBBR specification, users SHOULD be able to easily install the platform firmware themselves.
- MUST use GPT partition unless the SoC does not support it (fallback to MBR). The hardware vendor may provide an existing GPT partition table which MUST NOT be removed by the distro.
- MUST NOT touch protective partitions in the GPT partition table.
- MUST support UEFI as boot option, other options like extlinux may be supported, but that is not a part of EBBR spec.
- SHOULD provide means to update the platform firmware, preferably via a tool like fwupd

Complying with the EBBR specification is in your best interest as a distro since booting your distro will be much easier on any type of device 
complying with the EBBR specification and existing standards such as UEFI.
Of course you can still support other platform firmwares which may not follow standards, this can be easily supported in fwupd with firmware branches.
This way, power users still have all the features they want, we minimize the chance to brick a device, and be compliant with existing standards.

# Devices

| Device                           | Platform | Platform firmware location     |
| -------------------------------- | -------- | ------------------------------ |
| PinePhone Pro - Explorer Edition | rk3399s  | shared storage, eMMC           |
| PinePhone Pro - dev edition      | rk3399s  | shared storage, eMMC           |
| PineBook Pro                     | rk3399   | SPI flash                      |
| RockPro64                        | rk3399   | SPI flash                      |
| PinePhone                        | A64      | dedicated boot partition, eMMC |
| Pinebook                         | A64      | shared storage, eMMC           |
